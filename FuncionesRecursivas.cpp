// Author: Pablo arkadiusz Kalemba
// Date: 25-10-2019

#include <iostream>
#include <chrono>

//Math power
std::size_t pow_recursive(std::size_t m, unsigned n);
std::size_t pow_loop(std::size_t m, unsigned n);

//Taylor series
double taylor_series_recursive(std::size_t x, unsigned n);
double taylor_horner_series_recursive(std::size_t x, unsigned n);
double taylor_horner_series_loop(std::size_t x, unsigned n);
double taylor_horner_series_recursive_v2(std::size_t x, unsigned n);

//Fibonacci
std::size_t fibonacci_loop(unsigned n);
std::size_t fibonacci_recursive(unsigned n);

//Tower of Hanoi
void Tower_of_Hanoi(std::size_t num, std::string a, std::string b, std::string c);


//Binary search
std::size_t* binary_search_loop(std::size_t* begin, std::size_t* end, std::size_t num);
std::size_t* binary_search_recursive(std::size_t* begin, std::size_t* end, std::size_t num);

int main()
{
	using Timer = std::chrono::time_point<std::chrono::steady_clock>;
	using Duration = std::chrono::duration<float>;

	Timer start = std::chrono::high_resolution_clock::now();

	std::cout << "fib: " << fibonacci_recursive(10)<<"\n";

	Timer end = std::chrono::high_resolution_clock::now();
	Duration dur = end - start;
	std::cout << "duration : " << dur.count();
}

std::size_t pow_recursive(std::size_t m, unsigned n)
{

	return !n ? 1 : !(n % 2) ?
		pow_recursive(m * m, n / 2) :
		pow_recursive(m * m, (n - 1) / 2) * m;
}
std::size_t pow_loop(std::size_t m, unsigned n)
{
	int res = 1; // Initialize result 
	while (n) {
		if (n & 1) //si es impar
			res *= m;
		n >>= 1;// igual a n/=2
		m *= m; // Change x to x^2 
	}
	return res;
}

//slower version of taylor series || time complexity: 0(n^2)
double taylor_series_recursive(std::size_t x, unsigned n)
{
	static double p = 1, f = 1;
	if (!n) return 1;

	double value = taylor_series_recursive(x, n - 1);
	p *= x;
	f *= n;
	return value + p / f;
}

//time complexity: 0(n)
double taylor_horner_series_recursive(std::size_t x, unsigned n)
{
	static double f{ 0 };
	if (!n) return 1;
	++f;
	return 1 + x / f * taylor_horner_series_recursive(x, n - 1);
}
double taylor_horner_series_recursive_v2(std::size_t x, unsigned n)
{
	static double f{ 0 };
	if (!n) return 1;
	f = 1 + x / n * f;
	return taylor_horner_series_recursive_v2(x, n - 1);
}

//time complexity: 0(n) || space complexity: o(1)
double taylor_horner_series_loop(std::size_t x, unsigned n)
{
	double res{ 1 };
	for (; n > 0; --n)
		res = res * x / n + 1;
	return res;
}



std::size_t* binary_search_loop(std::size_t* const begin, std::size_t* const end, std::size_t elem)
{
	std::size_t* first{ begin }, * last{ end - 1 };
	std::size_t* middle;

	while (first <= last)
	{
		middle = begin + std::distance(begin, end) / 2;
		if (*middle == elem) return middle;
		if (elem > * middle)
			first = middle + 1;
		else
			last = middle - 1;
	}
	return nullptr;
}

std::size_t* binary_search_recursive(std::size_t* const begin, std::size_t* const end, std::size_t elem)
{
	std::size_t* first{ begin }, * last{ end - 1 };
	std::size_t* middle = begin + std::distance(begin, end) / 2;
	if (first > last) return nullptr;
	if (elem == *middle) return middle;
	return elem > * middle ?
		binary_search_recursive(middle + 1, last, elem) :
		binary_search_recursive(first, middle - 1, elem);
}

std::size_t fibonacci_loop(unsigned n)
{
	if (n < 2) return n;
	std::size_t  s{ 0 };
	for (std::size_t i{ 2 }, num_before{ 1 }; i <= n + 1; ++i)
	{
		std::size_t temp{ s };
		s += num_before;
		num_before = temp;
	}
	return s;
}

std::size_t fibonacci_recursive(unsigned n)
{
	size_t(&f) (unsigned) = fibonacci_recursive; //referencia a la funcion
	return (n < 2) ? n : f(n - 2) + f(n - 1);
}


void Tower_of_Hanoi(std::size_t num, std::string a, std::string b, std::string c)
{
	if (!num) return;
	Tower_of_Hanoi(num - 1, a, c, b);
	std::cout << "Mueve una ficha del " << a << " al " << c << std::endl;
	Tower_of_Hanoi(num - 1, b, a, c);
}